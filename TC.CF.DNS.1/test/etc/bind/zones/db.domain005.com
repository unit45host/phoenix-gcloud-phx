;
; BIND data file for local loopback interface
;
$TTL    604800
@       IN      SOA     ns1.domain005.com. admin.ns1.domain005.com. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
;@      IN      NS      localhost.
;@      IN      A       127.0.0.1
;@      IN      AAAA    ::1
        IN      NS      ns1.domain005.com.
        IN      NS      ns2.domain005.com.
; name servers - A records
ns1.domain005.com.          IN      A       10.128.10.11
ns2.domain005.com.          IN      A       10.128.20.12

; 10.0.0.0/16 - A records
host1.domain005.com.        IN      A      10.0.5.1
host2.domain005.com.        IN      A      10.0.5.2
host3.domain005.com.        IN      A      10.0.5.3