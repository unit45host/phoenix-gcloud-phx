echo hack $1

docker exec -it static_child00$1_1 sed -i 's/^www..*/;tmp_&/' /etc/bind/zones/db.linux.com && \
docker exec -it static_child00$1_1 sed -i '/^;www..*/s/^; *//' /etc/bind/zones/db.linux.com && \
docker exec -it static_child00$1_1 sed -i '/^;tmp_www..*/s/tmp_ *//' /etc/bind/zones/db.linux.com && \
docker exec -it static_child00$1_1 /etc/init.d/bind9 restart
# && docker exec -it static_child00$1_1 cat /etc/bind/zones/db.linux.com
