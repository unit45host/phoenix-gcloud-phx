#!/bin/bash

echo ishacked $1

i=1
N=${1:-"3"}

while [[ $i -le $N ]]; do

   ORI=$(docker exec -it static_child00${i}_1 cat /etc/bind/zones/db.linux.com | grep -oP '^www\..*;ori')
   # HACK=$(docker exec -it static_child00$1_1 cat /etc/bind/zones/db.linux.com| grep -oP '^;www\..*;hack')

   if [ -z "$ORI" ]
   then
      echo ${i}" - HACKED";
   else
      echo ${i}" - OK";
   fi
  i=$(($i + 1))
done