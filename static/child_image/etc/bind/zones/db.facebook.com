;
; BIND data file for local loopback interface
;
$TTL    604800
@       IN      SOA     ns1.domain001.com. admin.ns1.domain001.com. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@      IN      NS      facebook.com.
@      IN      A       31.13.65.36
;@      IN      AAAA    ::1
        IN      NS      b.ns.facebook.com.
        IN      NS      a.ns.facebook.com.
; name servers - A records
b.ns.facebook.com.          IN      A       69.171.255.12
a.ns.facebook.com.          IN      A       69.171.239.12
