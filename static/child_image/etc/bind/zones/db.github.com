;
; BIND data file for local loopback interface
;
$TTL    604800
@       IN      SOA     ns1.domain005.com. admin.ns1.domain005.com. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@      IN      NS      github.com.
@      IN      A       192.30.253.113
;@      IN      AAAA    ::1
        IN      NS      ns1.p16.dynect.net.
        IN      NS      ns2.p16.dynect.net.
        IN      NS      ns3.p16.dynect.net.
        IN      NS      ns4.p16.dynect.net.
; name servers - A records
ns1.p16.dynect.net.          IN      A       208.78.70.16
ns2.p16.dynect.net.          IN      A       204.13.250.16
ns3.p16.dynect.net.          IN      A       208.78.71.16
ns4.p16.dynect.net.          IN      A       204.13.251.16
