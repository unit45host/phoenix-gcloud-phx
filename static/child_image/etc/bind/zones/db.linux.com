;
; BIND data file for local loopback interface
;
$TTL    604800
@       IN      SOA     ns1.domain003.com. admin.ns1.domain003.com. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@      IN      NS      linux.com.
@      IN      A       23.185.0.3
;@       IN      A       167.86.89.68
;@      IN      AAAA    ::1
        IN      NS      ns1.dnsimple.com.
        IN      NS      ns2.dnsimple.com.
        IN      NS      ns3.dnsimple.com.
        IN      NS      ns4.dnsimple.com.

; name servers - A records
ns1.dnsimple.com.          IN      A       162.159.24.4
ns2.dnsimple.com.          IN      A       162.159.25.4
ns3.dnsimple.com.          IN      A       162.159.26.4
ns4.dnsimple.com.          IN      A       162.159.27.4

; others
www.linux.com.             IN      A       23.185.0.3     ;ori
;www.linux.com.             IN      A       167.86.89.68   ;hack
