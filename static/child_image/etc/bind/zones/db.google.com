;
; BIND data file for local loopback interface
;
$TTL    604800
@       IN      SOA     ns1.domain004.com. admin.ns1.domain004.com. (
                              3         ; Serial
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
@      IN      NS      google.com.
@      IN      A       172.217.164.174
;@      IN      AAAA    ::1
        IN      NS      ns1.google.com.
        IN      NS      ns2.google.com.
        IN      NS      ns3.google.com.
        IN      NS      ns4.google.com.
; name servers - A records
ns1.google.com.          IN      A       216.239.32.10
ns2.google.com.          IN      A       216.239.34.10
ns3.google.com.          IN      A       216.239.36.10
ns4.google.com.          IN      A       216.239.38.10
