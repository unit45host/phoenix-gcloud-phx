#!/bin/bash

echo "fix instances"

i=1
N=${1:-"3"}

while [[ $i -le $N ]]; do

   ORI=$(docker exec static_child00${i}_1 cat /etc/bind/zones/db.linux.com | grep -oP '^www\..*;ori')
   # HACK=$(docker exec -it static_child00$1_1 cat /etc/bind/zones/db.linux.com| grep -oP '^;www\..*;hack')

   if [ -z "$ORI" ]
   then
      (/usr/local/bin/docker-compose up -d --force-recreate child00${i})
      echo ${i}" - FIXED";
   else
      echo ${i}" - OK";
   fi
  i=$(($i + 1))
done
