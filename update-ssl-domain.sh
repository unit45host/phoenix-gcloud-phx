#!/bin/bash

DOMAIN='phoenix.polymerity.hu'
SUBDOMAIN='*.'${DOMAIN}
certbot certonly --manual --preferred-challenges dns -d $DOMAIN -d $SUBDOMAIN
mkdir -p /etc/ssl/certs
cat /etc/letsencrypt/live/$DOMAIN/fullchain.pem /etc/letsencrypt/live/$DOMAIN/privkey.pem > /etc/ssl/certs/$DOMAIN.pem
