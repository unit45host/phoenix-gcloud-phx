#!/bin/bash
key_folder=/etc/letsencrypt/live

/etc/init.d/haproxy stop

for DOMAIN in $(ls  $key_folder);  do
   echo "update domain ${DOMAIN}"
   certbot certonly  --keep-until-expiring --standalone --preferred-challenges http --http-01-port 80 -d $DOMAIN
   mkdir -p /etc/ssl/certs
   cat /etc/letsencrypt/live/$DOMAIN/fullchain.pem /etc/letsencrypt/live/$DOMAIN/privkey.pem > /etc/ssl/certs/$DOMAIN.pem
done

/etc/init.d/haproxy start

